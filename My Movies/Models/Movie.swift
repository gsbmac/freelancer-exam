//
//  Movie.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 14/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

struct Movie {
    
    var name        : String! = ""
    var rating      : String! = ""
    var channel     : String! = ""
    var end_time    : String! = ""
    var start_time  : String! = ""
    
}
