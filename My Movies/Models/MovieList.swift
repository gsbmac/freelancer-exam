//
//  MovieList.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 14/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

struct MovieList {
    
    var count       : Int! = 0
    var results     : [Movie] = []
    
}
