//
//  BaseViewController.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 14/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

class BaseViewContoller: UIViewController {
    
    var loadingContainer : UIView?
    var loadingIndicator : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func setBackButton(_ title: String) {
        
        let backItem = UIBarButtonItem()
        backItem.title = title
        navigationItem.backBarButtonItem = backItem
        
    }
    
    func showAlertView(_ title: String, message: String) {
        
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    
    func checkIfImageExists(_ fileName: String) -> Bool {
        
        // Checks if the file exists in the project directory
        // Reference: https://stackoverflow.com/questions/24181699/how-to-check-if-a-file-exists-in-the-documents-directory-in-swift
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(fileName)?.path
        let fileManager = FileManager.default
        
        return fileManager.fileExists(atPath: filePath!)
        
    }
    
}

public extension UIView {
    
    public class func fromNib(_ nibNameOrNil: String? = nil) -> Self {
        
        return fromNib(nibNameOrNil, type: self)
        
    }
    
    public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T {
        
        let v: T? = fromNib(nibNameOrNil, type: T.self)
        return v!
        
    }
    
    public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T? {
        
        var view: T?
        let name: String
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            // Most nibs are demangled by practice, if not, just declare string explicitly
            name = nibName
        }
        let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        for v in nibViews! {
            if let tog = v as? T {
                view = tog
            }
        }
        
        return view
        
    }
    
    public class var nibName: String {
        
        let name = "\(self)".components(separatedBy: ".").first ?? ""
        return name
        
    }
    
    public class var nib: UINib? {
        
        if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
            return UINib(nibName: nibName, bundle: nil)
        } else {
            return nil
        }
        
    }
    
}
