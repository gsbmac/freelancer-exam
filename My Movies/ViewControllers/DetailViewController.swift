//
//  DetailViewController.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 15/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

class DetailViewController: BaseViewContoller {
    
    var movieDetails: Movie!
    var mainView: DetailView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = movieDetails.name
        self.mainView = DetailView.fromNib("DetailView")
        
        self.view = self.mainView
        self.loadInitialData()
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func loadInitialData() {
        
        self.mainView.movieTitle.text = movieDetails.name
        self.mainView.movieSchedule.text = movieDetails.start_time+" - "+movieDetails.end_time
        self.mainView.ratingImageView.image = UIImage(named: movieDetails.rating)
        
    }
    
}
