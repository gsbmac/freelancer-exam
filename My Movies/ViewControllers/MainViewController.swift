//
//  MainViewController.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 14/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

class MainViewController: BaseViewContoller {
    
    var index: Int = 1
    var limit: Int = 1
    var mainView: MainView!
    var movieItems: [Movie] = []
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "My Movies"
        self.mainView = MainView.fromNib("MainView")
        self.view = self.mainView
        
        self.mainView.movieTableView.register(
            UINib(nibName: "MovieTableViewCell", bundle: Bundle.main),
            forCellReuseIdentifier: "movieCell"
        )
        
        self.mainView.movieTableView.delegate = self
        self.mainView.movieTableView.dataSource = self
        
        self.initializeRefreshControl()
//        self.initializePullRefresh()
        self.loadAllMovies(refreshControl)
        
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func initializeRefreshControl() {
        
        let indicatorFooter: UIActivityIndicatorView = UIActivityIndicatorView.init(
            frame: CGRect(x: 0, y: 0, width: self.mainView.frame.width, height: 44)
        )
        
        indicatorFooter.color = UIColor.black
        indicatorFooter.startAnimating()
        self.mainView.movieTableView.tableFooterView = indicatorFooter
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y + scrollView.frame.size.height == scrollView.contentSize.height) {
            self.index += 1
            self.loadAllMovies(refreshControl);
        }
        
    }
    
    func initializePullRefresh() {
        
        refreshControl.tintColor = UIColor(red: 36/255, green: 148/255, blue: 197/255, alpha: 1)
        refreshControl.addTarget(self, action: #selector(loadAllMovies(_:)), for: UIControlEvents.valueChanged)
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            self.mainView.movieTableView.refreshControl = refreshControl
        } else {
            self.mainView.movieTableView.addSubview(refreshControl)
        }
        
    }
    
    func loadAllMovies(_ refreshControl: UIRefreshControl) {
    
        APIConnector.getMovieList(self.index, onSuccess: { (response) -> Void in
                                
            if let JSON = response.result.value as! [String : AnyObject]? {
                self.limit = (JSON["count"]?.integerValue)!
                let resultItems = JSON["results"] as! [AnyObject]?

                for value in resultItems! {
                    let item = Movie(
                        name: (value as! NSDictionary).object(forKey: "name") as! String,
                        rating: (value as! NSDictionary).object(forKey: "rating") as! String,
                        channel: (value as! NSDictionary).object(forKey: "channel") as! String,
                        end_time: (value as! NSDictionary).object(forKey: "end_time") as! String,
                        start_time: (value as! NSDictionary).object(forKey: "start_time") as! String
                    )
                    
                    self.movieItems.append(item)
                }
                
                self.mainView.movieTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
            
        }, onFailure: { (response) -> Void in
            if let _ = response.result.value {
                self.showAlertView("Error", message: "Invalid data")
                self.refreshControl.endRefreshing()
            } else {
                self.showAlertView("Error", message: "Cannot connect to server")
                self.refreshControl.endRefreshing()
            }
        })

        
    }
    
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.movieItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell
        
        if cell == nil {
            cell = MovieTableViewCell()
        }
        
        let schedule: String = self.movieItems[indexPath.row].start_time+" - "+self.movieItems[indexPath.row].end_time
        let rating: String = self.checkIfImageExists(self.movieItems[indexPath.row].rating) ? "NR" : self.movieItems[indexPath.row].rating
        let channel: String = self.checkIfImageExists(self.movieItems[indexPath.row].channel) ? "One" : self.movieItems[indexPath.row].channel
        
        cell!.movieScheduleLabel.text = schedule
        cell!.movieTitleLabel.text = self.movieItems[indexPath.row].name
        cell!.ratingImageView.image = UIImage(named: rating)
        cell!.previewImageView.image = UIImage(named: channel)
        
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let destination: DetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailViewController") as UIViewController! as! DetailViewController
        destination.movieDetails = self.movieItems[indexPath.row]
        
        self.setBackButton("")
        navigationController?.pushViewController(destination, animated: true)
        
    }
    
}
