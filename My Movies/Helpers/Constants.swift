//
//  Constants.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 14/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

class Constants {
    
    static var URL_GET_MOVIE_LIST = "https://www.whatsbeef.net/wabz/guide.php"
    
    static var SCREEN_SIZE      = UIScreen.main.bounds
    static var SCREEN_WIDTH     = UIScreen.main.bounds.size.width
    static var SCREEN_HEIGHT    = UIScreen.main.bounds.size.height
    
}
