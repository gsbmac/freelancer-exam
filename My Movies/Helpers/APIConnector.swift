//
//  APIConnector.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 14/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import Alamofire

class APIConnector {
    
    static func getMovieList(_ params: Int,
                             onSuccess: @escaping (DataResponse<Any>) -> Void,
                             onFailure: @escaping (DataResponse<Any>) -> Void) {
        
        let url = Constants.URL_GET_MOVIE_LIST+"?start=\(params)"
        
        Alamofire.request(url).responseJSON { response in
            print(response.request ?? "")  // original URL request
            print(response.response ?? "") // HTTP URL response
            print(response.data ?? "")     // server data
            print(response.result)   // result of response serialization
            
            if response.response?.statusCode == 200 {
                onSuccess(response)
            } else {
                onFailure(response)
            }
            
        }
        
    }
    
}
