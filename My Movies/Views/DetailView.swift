//
//  DetailView.swift
//  My Movies
//
//  Created by Gilmar San Buenaventura on 15/08/2017.
//  Copyright © 2017 Gilmar San Buenaventura. All rights reserved.
//

import UIKit

class DetailView: BaseView {
    
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieSchedule: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    
}
